var camera, scene, renderer;
var effect, controls;
var element, container;
var showSphere, showOverview = false;

var clock = new THREE.Clock();

init();
animate();

function init() {
  renderer = new THREE.WebGLRenderer({antialias:true, alpha: true});
  element = renderer.domElement;
  container = document.getElementById('example');
  container.appendChild(element);

  if(stereo) effect = new THREE.StereoEffect(renderer);

  scene = new THREE.Scene();

  camera = new THREE.PerspectiveCamera(90, 1, 0.001, 700);
  camera.position.set(0, 10, 0);
  scene.add(camera);

  controls = new THREE.OrbitControls(camera, element);
  controls.rotateUp(Math.PI / 4);
  controls.target.set(
    camera.position.x + 0.1,
    camera.position.y,
    camera.position.z
  );

  controls.noZoom = true;
  controls.noPan = true;

  function setOrientationControls(e) {
    if (!e.alpha) {
      return;
    }

    controls = new THREE.DeviceOrientationControls(camera, true);
    controls.connect();
    controls.update();

    element.addEventListener('click', fullscreen, false);

    window.removeEventListener('deviceorientation', setOrientationControls, true);
  }
  window.addEventListener('deviceorientation', setOrientationControls, true);

  //***Light***
  var light = new THREE.HemisphereLight(0x777777, 0x000000, 1);
  scene.add(light);

  window.addEventListener('resize', resize, false);
  setTimeout(resize, 1);

}

function loadOverview(){
  cleanScene();
  showSphere = false;

  addText("press a number");

  var v00 = new THREE.Vector2(0,0);
  var v01 = new THREE.Vector2(0,1);
  var v10 = new THREE.Vector2(1,0);
  var v11 = new THREE.Vector2(1,1);


  var background = new THREE.MeshBasicMaterial({map: THREE.ImageUtils.loadTexture('images/stars.png')}); //backgroundcolor of overview-sphere
  var material  = new THREE.MeshFaceMaterial([background]); //add backgroundcolor to materials
  material.transparent = true;

  for(i = 0; i<rooms.length; i++){
    material.materials.push(new THREE.MeshBasicMaterial({map: THREE.ImageUtils.loadTexture("images/overview/"+rooms[i]), alphaMap: getNumber(i+1)})); //add all images to materials
  }

  var cube = new THREE.SphereGeometry(100, 5, 5); //set size of sphere and amound of grids

  var sphereGrid = 15; //startingpoint to assign images (max. 40)
  for(i = 1; i<=rooms.length; i++){
    cube.faces[sphereGrid].materialIndex = i; //sets position of texture
    cube.faces[sphereGrid+1].materialIndex = i;
    cube.faceVertexUvs[0][sphereGrid] = [v11.clone(), v01.clone(), v10.clone()]; //shrinks texture to grid size
    cube.faceVertexUvs[0][sphereGrid+1] = [v01.clone(), v00.clone(), v10.clone()];
    sphereGrid = sphereGrid + 2; //count up
  }

  overview = new THREE.Mesh(cube, material);
  overview.scale.x = -1 //texture on inside
  overview.name = "overview";

  scene.add(overview);
  showOverview = true;
}

function loadRoom(roomNr){
  //***Single Sphere***
  cleanScene();
  showOverview = false;

  var sphere = new THREE.Mesh(
    new THREE.SphereGeometry(100, 32, 32),
    new THREE.MeshBasicMaterial({
      map: THREE.ImageUtils.loadTexture('images/'+rooms[roomNr])
    })
  );

  sphere.name = "sphere";
  sphere.scale.x = -1

  scene.add(sphere);

  //***Back Button***
  addText("press o to get to overview");
  showSphere = true;
}

function cleanScene(){
  if(showSphere){
    removeFromScene("sphere");
  }else if(showOverview){
    removeFromScene("overview");
  }
  removeFromScene("textMesh");
}

function removeFromScene(name){
  var key = scene.getObjectByName(name);
  scene.remove(key);
}

function addText(text){
  var material = new THREE.MeshPhongMaterial({
    color: 0x0033ff, specular: 0x555555, shininess: 30
  });

  var textGeom = new THREE.TextGeometry(text, {
      font: 'helvetiker',
      size: 2,
      height: 1
  });

  var textMesh = new THREE.Mesh(textGeom, material);
  textMesh.position.x = 10
  textMesh.position.y = 5;
  textMesh.position.z = 10;
  textMesh.rotation.y =  - Math.PI;
  textMesh.name = "textMesh";

  scene.add(textMesh);
}

loadOverview();

$(document).keydown(function(e){
  var s = String.fromCharCode(e.which);
  if(s.match(/[0-9]/) && s<=rooms.length) loadRoom(s-1);
  else if(s.match = "o") loadOverview();
});

function getNumber(number){
  var bitmap = document.createElement('canvas');
  bitmap.width = 512;
  bitmap.height = 256;

  var g = bitmap.getContext('2d');

  g.fillStyle = "white";
  g.fillRect(0, 0, bitmap.width, bitmap.height);
  g.font = '50px Arial';
  g.fillStyle = 'black';
  g.fillText(number, 20, 50);
  g.strokeStyle = 'black';


  var texture = new THREE.Texture(bitmap);
  texture.needsUpdate = true;
  return texture;
}

/***get the camera orientation, we need to call it when the viewing-angle changes***/
function getCamera(){
  var pLocal = new THREE.Vector3(0, 0, -1); // pick a point in front of the camera
  var pWorld = pLocal.applyMatrix4(camera.matrixWorld); // transform that point into world space
  var dir = pWorld.sub(camera.position).normalize(); // construct the desired direction vector
  console.log(dir);
}

function resize() {
  var width = container.offsetWidth;
  var height = container.offsetHeight;

  camera.aspect = width / height;
  camera.updateProjectionMatrix();

  renderer.setSize(width, height);
  if(stereo) effect.setSize(width, height);
}

function update(dt) {
  resize();
  camera.updateProjectionMatrix();
  controls.update(dt);
}

function render(dt) {
  if(stereo) effect.render(scene, camera);
  else if(!stereo) renderer.render(scene, camera);
}

function animate(t) {
  requestAnimationFrame(animate);
  update(clock.getDelta());
  render(clock.getDelta());
}

function fullscreen() {
  if (container.requestFullscreen) {
    container.requestFullscreen();
  } else if (container.msRequestFullscreen) {
    container.msRequestFullscreen();
  } else if (container.mozRequestFullScreen) {
    container.mozRequestFullScreen();
  } else if (container.webkitRequestFullscreen) {
    container.webkitRequestFullscreen();
  }
}