        <footer class="page-footer">
            <div class="footer-copyright">
            <!-- Das ist der Footer der auf der normalen Desktop-width angezeigt wird -->
                <div class="container footer_respo_over">
                    &copy; 2015 Marco Kudala | Stephan Czieslok | Robert Krause | Simon Stubenvoll
                    <a class="grey-text text-lighten-4 right" href="https://www.th-deg.de" target="_blank">TH-Deggendorf</a>
                </div>
                <!-- Bei einer in der CSS-File definierten Browser-width wird der Footer mit der Klasse .footer_respo_over eingeblendet und der Footer 
                mit der Klasse .footer_respo eingeblendet -->
                <div class="container footer_respo">
                    &copy; 2015 MK | SC | RK | ST
                    <a class="grey-text text-lighten-4 right" href="https://www.th-deg.de" target="_blank">TH-Deggendorf</a>
                </div>
            </div>
        </footer>

        <!--Import jQuery before materialize.js-->
        <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
        <script type="text/javascript" src="js/materialize.min.js"></script>
        
        <!-- Dieses Skript spricht die Function .sideNav() im .js von materialize an, 
        damit der schöne Effekt einer seitlich ausfahrbaren Navigation ausgeführt werden kann -->
        
        <script type="text/javascript">

            $(document).ready(function () {
                $(".button-collapse").sideNav();
            });

        </script>
    </body>
</html>