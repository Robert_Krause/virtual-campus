<!DOCTYPE html>
<html>
    <head>
        <title>Virtual Campus</title>
        
        <!--Import Google Icon Font-->
        <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <!--Import materialize.css-->
        <link type="text/css" rel="stylesheet" href="css/materialize.css"  media="screen,projection"/>
        <!-- Import own Styles -->
        <link type="text/css" rel="stylesheet" href="css/style.css"  media="screen,projection"/>

        <!--Let browser know website is optimized for mobile-->
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0">
        <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
        <link rel="icon" href="images/favicon.ico" type="image/x-icon">
    </head>

    <body>
    <!-- Das DIV mit der ID=splashscreen wird dem Nutzer für 3 Sekunden angezeigt und dann 
    durch eine Animation nach oben hin ausgeblendet -->
        <div id="splashscreen" class="valign-wrapper" style="border-bottom: 2px solid;">
            <div class="valign row center-align">
                <img class="responsive-img" src="images/logo_startseite/mit_vr.png" alt="VR-Brille">
            </div>
        </div>

        <header>
            <nav>
                <div class="nav-wrapper container">
                    <a href="index.php" class="brand-logo">Virtual-Campus</a>
                    <a href="index.php" data-activates="mobile-demo" class="button-collapse"><i class="material-icons">menu</i></a>
                    <ul class="right hide-on-med-and-down">
                        <li><a href="project.php">Projektbeschreibung</a></li>
                        <li><a href="team.php">Das Team</a></li>
                    </ul>
                    <ul class="side-nav" id="mobile-demo">
                        <li><a href="project.php">Projektbeschreibung</a></li>
                        <li><a href="team.php">Das Team</a></li>
                    </ul>
                </div>
            </nav>
        </header>