        <footer class="page-footer">
            <div class="footer-copyright">
                <div class="container footer_respo_over">
                    &copy; 2015 Marco Kudala | Stephan Czieslok | Robert Krause | Simon Stubenvoll
                    <a class="grey-text text-lighten-4 right" href="https://www.th-deg.de" target="_blank">TH-Deggendorf</a>
                </div>
                <div class="container footer_respo">
                    &copy; 2015 MK | SC | RK | ST
                    <a class="grey-text text-lighten-4 right" href="https://www.th-deg.de" target="_blank">TH-Deggendorf</a>
                </div>
            </div>
        </footer>

        <!--Import jQuery before materialize.js-->
        <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
        <script type="text/javascript" src="js/materialize.min.js"></script>
        <!--Script for the awesome sidenav-->
        <script type="text/javascript">

            $(document).ready(function () {
                $(".button-collapse").sideNav();
                
                /* Durch die .slideUp() wird das DIV mit der ID=splashscreen nach 3 Sekunden nach oben hin ausgeblendet.
                Die Attribute "slow" und "easeInOutQuart" sind weitere Eigenschaften der slideUp(), die die Animation des DIVs 
                verändern. */
                setTimeout(function(){
                    $("#splashscreen").slideUp("slow", "easeInOutQuart");
                }, 3000);
            });

        </script>
    </body>
</html>