﻿
<?php
include 'includes/header.php'
?>

<main>
    <div class="container">
        <div class="row">
            <div class="col s12" style="text-align: center; margin-bottom: 20px;">
                <span class="flow-text">Team Virtual-Campus</span>
            </div>
        </div>
        <div class="row">
            <div class="col s6">
                <div class="card-panel grey lighten-5 z-depth-1 hoverable">
                    <div class="row valign-wrapper">
                        <div class="col s4">
                            <img src="images/team/marco.jpg" alt="" class="circle responsive-img"> <!-- notice the "circle" class -->
                        </div>
                        <div class="col s12">
                            <span class="black-text">
                                B. Eng. Marco Kudala (27) </br> Entwicklung Frontend </br></br> B. Eng. Medientechnik, spezialisiert in Webdesign und Webprogrammierung</br>m.kudala@oth-aw.de
                            </span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col s6">
                <div class="card-panel grey lighten-5 z-depth-1 hoverable">
                    <div class="row valign-wrapper">
                        <div class="col s4">
                            <img src="images/team/stephan.jpg" alt="" class="circle responsive-img"> <!-- notice the "circle" class -->
                        </div>
                        <div class="col s12">
                            <span class="black-text">
                                B. Eng. Stephan Czieslok (24) </br> Fotografie & Dokumentation</br></br> B. Eng. Medientechnik, spezialisiert in Marken- und Unternehmenskommunikation</br>s.czieslok@oth-aw.de
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col s6">
                <div class="card-panel grey lighten-5 z-depth-1 hoverable">
                    <div class="row valign-wrapper">
                        <div class="col s4">
                            <img src="images/team/robert.jpg" alt="" class="circle responsive-img"> <!-- notice the "circle" class -->
                        </div>
                        <div class="col s12">
                            <span class="black-text">
                                B. Eng. Robert Krause (24) </br> Web-App Entwicklung </br></br> B. Eng. Medientechnik, spezialisiert in Webdevelopment und WebApps</br>r.krause@oth-aw.de
                            </span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col s6">
                <div class="card-panel grey lighten-5 z-depth-1 hoverable">
                    <div class="row valign-wrapper">
                        <div class="col s4">
                            <img src="images/team/simon.jpg" alt="" class="circle responsive-img"> <!-- notice the "circle" class -->
                        </div>
                        <div class="col s12">
                            <span class="black-text">
                                B. Eng. Simon Stubenvoll (22) </br> Web-App Entwicklung </br></br> B. Eng. Medientechnik, spezialisiert in Mediengestaltung Digital & Print</br>s.stubenvoll@oth-aw.de
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>

<?php
include 'includes/footer.php'
?>