
<?php
include 'includes/header_index.php'
?>
<main>
    <div class="container">
        <div class="row">
            <div class="col s12" style="text-align: center; margin-bottom: 20px;">
                <span class="flow-text">Bitte wählen Sie, ob eine VR-Brille verwendet wird, oder nicht.</span>
            </div>
        </div>
        <div class="row">
            <div class="col s6" style="text-align: center;">  
                <!-- Passing through stereo = true -->
                <!-- Hier wird auf die Variable "stereo" in der app.html zugegriffen und auf "true" gesetzt,
                dadurch wird ein Splitscreen für eine VR-Brille aktiviert -->
                <a href="app.html?stereo=true">
                    <div class="card grey lighten-5 z-depth-1 hoverable">
                        <div class="card-image">
                            <img class="responsive-img" src="images/mit_vr.png" alt="VR-Brille">
                        </div>
                        <div class="card-content">
                            <p>Mit einer VR-Brille</p>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col s6" style="text-align: center;">
                <!-- Passing through stereo = false -->
                <!-- Hier wird auf die Variable "stereo" in der app.html zugegriffen und auf "false" gesetzt,
                dadurch kann der Virtual-Campus ohne eine VR-Brille erkundet werden -->
                <a href="app.html?stero=false">
                    <div class="card grey lighten-5 z-depth-1 hoverable">
                        <div class="card-image">
                            <img class="responsive-img" src="images/ohne_vr.png" alt="VR-Brille">
                        </div>
                        <div class="card-content">
                            <p>Ohne eine VR-Brille<p>
                        </div>
                    </div>
                </a>
            </div>
        </div>
    </div>
</main>

<?php
include 'includes/footer_index.php'
?>