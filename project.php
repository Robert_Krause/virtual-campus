
<?php
include 'includes/header.php'
?>

<main>
    <div class="container">
        <div class="row">
            <div class="col s12" style="text-align: center; margin-bottom: 20px;">
                <span class="flow-text">Ein virtueller Rundgang am Campus der TH-DEG</span>
            </div>
            <div class="projekt-beschreibung col s12">

                        <!-- Neuer <p>-Tag = neuer Absatz -->

                <p class="p-lead">
                    Die Gruppe 4 bestehend aus Stephan Czieslok, Robert Krause, Marco Kudala und Simon Stubenvoll,
                    mit dem Thema "Virtual-Campus" im Fach 3D-Computeranimation bei Prof. Dr. Ing. Peter Faber.
                </p>
                <p class="p-beschreibung">
                    So absurd die Idee auch klingen mag, sich ein Smartphone vor den Kopf zu schnallen: Sie ist wahnsinnig erfolgreich. Fast t&auml;glich kommen neue VR-Apps heraus - f&uuml;r Android gibt es bereits mehr als 200, f&uuml;r iOS sind es etwas weniger. Dabei handelt es sich nicht nur um schnelle Programmier&uuml;bungen von VR-Freaks - auch gro&szlig;e Firmen sind schon auf den Virtual-Reality-Zug aufgesprungen; unter anderem haben Mercedes Benz und Volvo Apps f&uuml;r umgeschnallte Smartphones ver&ouml;ffentlicht.
                </p>
                <p class="p-beschreibung">
                    Wir, die Gruppe 4 des Moduls 3D-Computer Animation, springen auch auf diesen Zug auf und entwickeln ein eigenes Konzept, die Google Cardboard Brille f&uuml;r &uuml;berzeugenede 3D-Visualisierungen einzusetzen.
                    Mit Hilfe der Developper-Tools "WebGL" und "Three.js" soll basierend auf dem Google Chrome - Basic VR Experiment - eine Web-App entstehen, die f&auml;hig ist, verschiedenste R&auml;ume und Bereiche des Campus der TH-Deggendorf zu pr&auml;sentieren.
                    Anfangs wird die WebApp mit ersten Photosphere Aufnahmen des Campus gespickt, die so &auml;hnlich aussehen wie die von Google ver&ouml;ffentlichten Street-View Bilder. Sp&auml;ter sollen die Studenten und Nutzer selbst aktiv werden und eigene Aufnahmen in eine Galerie hochladen k&ouml;nnen, um nach und nach den virtuellen Campus zu vervollst&auml;ndigen.
                    Mit Hilfe einer kleinen, via Bluetooth verbundenen Tastatur, k&ouml;nnen sich die Nutzer virtuell von einer Aufnahme zur n&auml;chsten bewegen und ihren Campus-Rundgang steuern. Zudem enthalten die Aufnahmen, Text, der in dreidimensionaler Form im Bild implementiert ist und Informationen zum aktuellen Ort oder anderen Funktionen gibt.
                </p>
                <p class="p-beschreibung">
                    Eingepflegt wird die Web-App in ein responsives Frontend, um die Campus Aufnahmen sowohl am Smartphone als auch auf dem Desktop Computer in optimaler Größe darzustellen. Auf der Website besteht die M&ouml;glichkeit die Photosphere Aufnhamen in zwei Varianten zu betrachten. Besitzt man eine VR-Brille mit Smartphone Halterung so kann man die Photosphere Aufnhame hierdurch betrachten. 
                    M&ouml;chte man das Bild auf dem Computer ansehen, besteht die M&ouml;glichkeit, das Panoramabild ohne 3D-Effekt darzustellen. Abgerundet wird das Frontend durch eine knappe Projektbeschreibung und der Vorstellung der beteiligten Teammitglieder.
                    Bleiben am Ende nur einige Fragen offen: Was sind die Developper-Tools WebGL und Three.js? Und was ist &uuml;berhaupt eine Photosphere? Die Beantwortung dieser Fragen und die konkrete Umsetzung der Web-App und des zugeh&ouml;rigen Frontends finden Sie in unserer Projektdokumentation. Viel Spa&szlig; beim Erkunden des Campus der Technischen Hochschule Deggendorf. (sc)
                </p>
            </div>
        </div>
    </div>
</main>

<?php
include 'includes/footer.php'
?>